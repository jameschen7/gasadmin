import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Members from '@/components/Members'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/members',
      name: 'Members',
      component: Members
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
